## Distribución de bombillos

### Requisitos

>Node version 16.3.0

### Correr proyecto

1.- Clonar este repositorio

2.- `npm i`

3.- `npm start`

4.- Abrir browser `http://localhost:4000`

5.- Abrir el archivo matrix.txt para modificar la matriz, luego refrescar el browser para ver los nuevos resultados