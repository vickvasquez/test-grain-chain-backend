const DIRECTION_ROOMS = {
  up: 'up',
  down: 'down',
  left: 'left',
  rigth: 'right'
};

const ROOM_WITHOUT_WALL = 0;

const getArrayWithSmallestLength = matrix => {
  return matrix.reduce((prev, current) => current.length < prev.length ? current : prev, matrix[0]);
};

const hasAdjacentRooms = (roomsToLight, direction) => {
  return !!roomsToLight.find(room => room.row === direction.row && room.column === direction.column);
};

const getCoordinateOfRoomsToLight = matrix => {
  let coordinatesOfRoomsToLight = [];

  for (let row = 0; row < matrix.length; row++) {
    for (let column = 0; column < matrix[row].length; column++) {
      if (matrix[row][column] === ROOM_WITHOUT_WALL) {
        coordinatesOfRoomsToLight = [...coordinatesOfRoomsToLight, { row, column }];
      }
    }
  }

  return coordinatesOfRoomsToLight;
};

const getAdjacentsRoomByDirection = ({ row, column, roomsToLight, direction }) => {
  let adjacentRooms = [];

  for (const [index] of roomsToLight.entries()) {
    let coordinateNextRoom = { row: row - (index + 1), column };

    if (direction === DIRECTION_ROOMS.down) {
      coordinateNextRoom = { row: row + (index + 1), column };
    }

    if (direction === DIRECTION_ROOMS.left) {
      coordinateNextRoom = { row, column: column - (index + 1) };
    }

    if (direction === DIRECTION_ROOMS.rigth) {
      coordinateNextRoom = { row, column: column + (index + 1) };
    }

    const adjacentRoom = roomsToLight.find(r => r.row === coordinateNextRoom.row && r.column === coordinateNextRoom.column);

    if (!adjacentRoom) break;

    adjacentRooms = [...adjacentRooms, adjacentRoom];
  }

  return adjacentRooms;
};

const getCoordinatesAdjacentsRooms = (coordinatesOfRoomsToLight) => {
  return coordinatesOfRoomsToLight.map(({ row, column }) => {
    const coordinateRoomUp = { row: row - 1, column };
    const coordinateRoomDown = { row: row + 1, column };
    const coordiNateRoomLeft = { row, column: column - 1 };
    const coordinateRoomRight = { row, column: column + 1 };

    const hasRoomAbove = hasAdjacentRooms(coordinatesOfRoomsToLight, coordinateRoomUp);
    const hasRoomDown = hasAdjacentRooms(coordinatesOfRoomsToLight, coordinateRoomDown);
    const hasRoomLeft = hasAdjacentRooms(coordinatesOfRoomsToLight, coordiNateRoomLeft);
    const hasRoomRight = hasAdjacentRooms(coordinatesOfRoomsToLight, coordinateRoomRight);

    let roomsDown = [];
    let roomsUp = [];
    let roomsLeft = [];
    let roomsRight = [];

    if (hasRoomDown) {
      roomsDown = getAdjacentsRoomByDirection({
        row,
        column,
        roomsToLight: coordinatesOfRoomsToLight,
        direction: DIRECTION_ROOMS.down
      });
    }

    if (hasRoomAbove) {
      roomsUp = getAdjacentsRoomByDirection({
        row,
        column,
        roomsToLight: coordinatesOfRoomsToLight,
        direction: DIRECTION_ROOMS.up
      });
    }

    if (hasRoomLeft) {
      roomsLeft = getAdjacentsRoomByDirection({
        row,
        column,
        roomsToLight: coordinatesOfRoomsToLight,
        direction: DIRECTION_ROOMS.left
      });
    }

    if (hasRoomRight) {
      roomsRight = getAdjacentsRoomByDirection({
        row,
        column,
        roomsToLight: coordinatesOfRoomsToLight,
        direction: DIRECTION_ROOMS.rigth
      });
    }

    return {
      room: { row, column },
      roomsTotal: roomsDown.length + roomsUp.length + roomsLeft.length + roomsRight.length,
      roomsDown,
      roomsUp,
      roomsLeft,
      roomsRight
    };
  })
    .sort((a, b) => b.roomsTotal - a.roomsTotal);
};

const getUnlitRooms = (lightedRooms, roomsToLight) => {
  return roomsToLight
    .filter(({ room }) => !lightedRooms
      .find(lightedRoom => (lightedRoom.row === room.row && room.column === lightedRoom.column))
    );
};

const getTotalLightBulbPerRoom = (roomsWithAdjacents, lightBulbs = [], currentRoom = null, position) => {
  const roomsToAnlyze = [...roomsWithAdjacents];

  if (currentRoom !== null) {
    roomsToAnlyze.splice(position, 1);
  }

  const [firstRoom = {}, ...rooms] = roomsToAnlyze;
  const roomSelected = currentRoom || firstRoom;
  const { room, roomsDown, roomsUp, roomsLeft, roomsRight } = roomSelected;

  const unlitRooms = getUnlitRooms([...roomsDown, ...roomsUp, ...roomsLeft, ...roomsRight], rooms);

  lightBulbs = [...lightBulbs, room];

  if (unlitRooms.length > 0) {
    return getTotalLightBulbPerRoom(unlitRooms, lightBulbs);
  }

  return lightBulbs;
};

const calculateDistributionCoordinates = roomsToLight => {
  const arrayOfLightBulbsPerRoom = roomsToLight.map((room, index) => getTotalLightBulbPerRoom(roomsToLight, [], room, index));

  return getArrayWithSmallestLength(arrayOfLightBulbsPerRoom);
};

module.exports = matrix => {
  const coorginatesOfRoomsToLight = getCoordinateOfRoomsToLight(matrix);
  const coordinateAdjacentRooms = getCoordinatesAdjacentsRooms(coorginatesOfRoomsToLight);

  return calculateDistributionCoordinates(coordinateAdjacentRooms);
};
