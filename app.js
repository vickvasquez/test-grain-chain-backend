const express = require('express');

const routes = require('./routes');

const app = express();

routes(app);

app.listen(4000, () => {
  console.log(`
    [INFO]: Listening on port 4000
    [INFO]: Open browser http://localhost:4000
  `);
});
