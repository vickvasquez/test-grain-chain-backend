const path = require('path');
const fs = require('fs');

const getLightBulbDistribution = require('../lib/get-ligth-bulb-distribution');

const filePath = path.resolve(__dirname, '../matrix.txt');

module.exports = app => {
  app.get('/', (_, res) => {
    res.sendFile(path.resolve('./views/index.html'));
  });

  app.get('/get-ligth-bulb-distribution', (_, res) => {
    const contentFile = fs.readFileSync(filePath, 'utf-8');
    const matrix = contentFile.split('\n')
      .map(line => line
        .trim()
        .split('')
        .map(n => parseInt(n, 10))
      );

    const coordinatesLightBulbs = getLightBulbDistribution(matrix);
    const result = placeLightBulbs(matrix, coordinatesLightBulbs);

    res.json({ data: result });
  });

  const placeLightBulbs = (matrix, coordinatesLightBulbs) => {
    coordinatesLightBulbs.forEach(foco => {
      matrix[foco.row][foco.column] = 'b';
    });

    return matrix;
  };
};
